from tkinter import *
from tkinter import ttk
from DBConnect import DBConnect

dbconnect = DBConnect()
class List:
    def __init__(self):
        self.root = Tk()
        self.dbconnent = DBConnect()
        table=ttk.Treeview(self.root)
        table.pack()
        table.heading('#0',text='ID')
        table.configure(column=('#Name' , '#Gender' , '#Comment'))
        table.heading('#Name', text='Name')
        table.heading('#Gender', text='Gender')
        table.heading('#Comment', text='Comment')
        cur =  dbconnect.GetData()
        i = 1
        for x in cur:
            table.insert('','end','#{}'.format(str(i)), text=str(i))
            table.set('#{}'.format(str(i)),'#Name',x['Name'])
            table.set('#{}'.format(str(i)), '#Gender', x['Gender'])
            table.set('#{}'.format(str(i)), '#Comment', x['Comment'])
            i+=1

        self.root.mainloop()