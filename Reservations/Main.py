from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from DBConnect import DBConnect
from ListRequest import List
dbconnect = DBConnect()

root = Tk()
root.title("Ticket Reservations")
root.configure(background='#e1d8b2')
#Style
style = ttk.Style()
style.theme_use('classic')
style.configure('TLabel' ,background='#e1d8b2')
style.configure('TButton' ,background='#e1d8b2')
style.configure('TRadiobutton' ,background='#e1d8b2')
#ful name
ttk.Label(root , text = "Full Name: ").grid(row = 0 , column=0,padx=10,pady=10)
EntryFullName= ttk.Entry(root,width=30,font=('Arial',16))
EntryFullName.grid(row=0,column=1,columnspan=2 , pady=10)
#gender
ttk.Label(root , text = "Gender: ").grid(row = 1 , column=0)
Gender = StringVar()
ttk.Radiobutton(root,text="Male",variable=Gender,value="Male").grid(row=1,column=1)
ttk.Radiobutton(root,text="Female",variable=Gender,value="Female").grid(row=1,column=2)
#Comment
ttk.Label(root,text="Comment:").grid(row=2,column=0)
txtComment = Text(root,width=30,height=15,font=('Arial',16))
txtComment.grid(row=2,column=1,columnspan=2)

#buttons
BSubmit = ttk.Button(root,text="Submit")
BSubmit.grid(row=3,column=3)
BList = ttk.Button(root,text="List Res.")
BList.grid(row=3,column=2)
def BuSave():
    msg=dbconnect.Add(EntryFullName.get(),Gender.get(),txtComment.get(1.0,'end'))
    messagebox.showinfo(title="Add",message=msg)
    EntryFullName.delete(0,'end')
    txtComment.delete(1.0,'end')
BSubmit.configure(command=BuSave)

def BuList():
    listrequest = List()
BList.configure(command=BuList)

root.mainloop()